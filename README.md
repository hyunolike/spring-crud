# Spring Boot CRUD 개발
> Spring Boot, JPA, Thymeleaf, MySQL, Unit Test

## 목차
1. [구현 방법](#1-구현-방법)
2. [실행 방법](#2-실행-방법)

## 1 구현 방법
### 1-1. 환경 및 구조
- 기술
  - Spring Boot Web
  - Spring Data JPA
  - MySQL Database
  - Thymeleaf
  - HTML5 & Bootstrap
- 소프트웨어 프로그램
  - Java 11
  - Intellij IDEA
  - MySQL Community Server
  - MySQL Workbench
### 1-2. 코드 구현
1. 스프링 부트 프로젝트 생성 및 셋업
2. Home Page
3. UserEntity & UserRepository
4. UserRepository UnitTest
5. 유저 기능 페이지 개발
6. 유저 생성
7. 유저 수정
8. 유저 삭제

## 2 실행 방법
![](img/endpoint.jpg)
